import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { AuthService } from './auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public isUserAuthenticated$: Observable<boolean>;

  constructor(
    private readonly _authService: AuthService
  ) { }

  ngOnInit() {
    this.isUserAuthenticated$ = this._authService.isUserAuthenticated();
  }
}
