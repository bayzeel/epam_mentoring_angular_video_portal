import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';

import { AuthService } from '../auth.service';
import { UserInterface } from '../layout/header/user.interface';
import { AppStateInterface } from '../store/app.state';
import * as authActions from '../store/actions/auth.actions';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public email: string;
  public password: string;
  public form: FormGroup;

  constructor(
    private readonly _authService: AuthService,
    private readonly _store: Store<AppStateInterface>,
    private readonly _fb: FormBuilder,
  ) {
    this.createForm();
  }

  ngOnInit() { }

  forgotPassword() {
    console.log('Forgot password');
  }

  login(): void {
    const user: UserInterface = {
      email: this.form.controls.email.value,
      password: this.form.controls.password.value,
    };

    this._store.dispatch(authActions.beginGetUserTokenAction({ user }));
  }

  public createForm() {
    this.form = this._fb.group( {
      email: ['', [Validators.required]],
      password: ['', [Validators.required]]
    } );
  }
}
