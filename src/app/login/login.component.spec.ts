import { async, ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';

import { ButtonModule } from '../shared/form-elements/button/button.module';

import { LoginComponent } from './login.component';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        LoginComponent,
      ],
      imports: [
        ButtonModule,
        FormsModule,
        RouterTestingModule,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    spyOn(window.console, 'log');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should output console.log when forgotPassword method run', () => {
    component.forgotPassword();

    expect(window.console.log).toHaveBeenCalledWith('Forgot password');
  });

  it('should navigate to \'courses\' route', inject([Router], (router: Router) => {
    spyOn(router, 'navigate').and.stub();
    component.login();

    expect(router.navigate).toHaveBeenCalledWith(['courses']);
  }));
});
