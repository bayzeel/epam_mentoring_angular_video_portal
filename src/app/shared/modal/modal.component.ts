import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
  ViewEncapsulation,
  ElementRef
} from '@angular/core';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ModalComponent implements OnInit, AfterViewInit {
  @Input() title: string;
  @Output() closeModal = new EventEmitter();
  @ViewChild('content', { read: ElementRef, static: true }) content: ElementRef;

  constructor() { }

  ngOnInit() {}

  ngAfterViewInit() { }

  handleDialogCardCancelClick() {
    this.closeModal.emit();
  }
}
