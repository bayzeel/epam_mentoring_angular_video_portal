import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { InputChipsComponent } from './input-chips.component';

@NgModule({
  declarations: [
    InputChipsComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
  ],
  exports: [
    InputChipsComponent,
  ],
})
export class InputChipsModule { }
