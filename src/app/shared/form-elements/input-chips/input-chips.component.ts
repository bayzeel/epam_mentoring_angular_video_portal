import { Component, EventEmitter, forwardRef, HostBinding, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { ControlValueAccessor, FormControl, NG_VALIDATORS, NG_VALUE_ACCESSOR, ValidationErrors, Validator } from '@angular/forms';
import { Subject, Subscription } from 'rxjs';
import { bufferCount, map } from 'rxjs/operators';

@Component({
  selector: 'app-input-chips',
  templateUrl: './input-chips.component.html',
  styleUrls: ['./input-chips.component.scss'],
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => InputChipsComponent),
      multi: true
    },
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputChipsComponent),
      multi: true
    }
  ]
})
export class InputChipsComponent implements OnInit, OnDestroy, ControlValueAccessor, Validator {
  private _searchTextSub: Subscription;
  private _searchedList: {name: string}[] = [];
  private _id = '';

  @HostBinding('attr.id') public externalId = '';

  @Input() public control: FormControl;
  @Input() public title: string;
  @Input('items') public set items(list: {name: string}[]) {
    this.selectedItems = list;
  }
  public get searchedList(): {name: string}[] {
    return this._searchedList;
  }
  @Input('searchedList') public set searchedList(list: {name: string}[]) {
    if (list && list.length) {
      this._searchedList = list.filter(item => {
        for (const selectedItem of this.selectedItems) {
          if (selectedItem.name === item.name) {
            return false;
          }
        }

        return true;
      });
    } else {
      this._searchedList = [];
    }
  }

  @Input()
  set id(value: string) {
    this._id = value;
    this.externalId = null;
  }

  get id() {
    return this._id;
  }

  @Input('value') private _value: string;

  get value() {
    return this._value;
  }

  set value(value) {
    this._value = value;
    this.onChange(value);
    this.onTouched();
    if (this._value) {
      this.search.emit(value);
    }
  }
  @Output() public search = new EventEmitter<string>();
  @Output() public shareSelectedItems = new EventEmitter<{name: string}[]>();

  public filterSearch = new Subject<string>();
  public selectedItems: {name: string}[] = [];

  static authorsValidator(control: FormControl, selectedItems: {name: string}[]) {
    if (control && selectedItems && !selectedItems.length) {
      return {authorsRequired: 'At least one author should be'};
    }
  }

  constructor() { }

  onChange: any = () => {};
  onTouched: any = () => {};

  ngOnInit() {
    this._searchTextSub = this.filterSearch
      .pipe(
        bufferCount(1),
        map(textArr => textArr[textArr.length - 1])
      )
      .subscribe(text => {
        if (text.trim()) {
          this.value = text;
        } else {
          this._searchedList = [];
        }
      });
  }

  public findText() {
    this.filterSearch.next(this.value);
  }

  public selectSearchedItem(searchedItem: {name: string}) {
    const index = this.selectedItems.indexOf(searchedItem);
    if (index === -1) {
      this.selectedItems.push(searchedItem);
      this._searchedList = [];
      this.shareSelectedItems.emit(this.selectedItems);
      this.control.updateValueAndValidity();
    }
  }

  deleteSelectedItemFromChips(selectedItem) {
    const index = this.selectedItems.indexOf(selectedItem);
    this.selectedItems.splice(index, 1);
    this.shareSelectedItems.emit(this.selectedItems);
    this.control.updateValueAndValidity();
  }

  ngOnDestroy() {
    if (this._searchTextSub) {
      this._searchTextSub.unsubscribe();
    }
  }

  registerOnChange(fn): void {
    this.onChange = fn;
  }

  registerOnTouched(fn): void {
    this.onTouched = fn;
  }

  writeValue(value): void {
    if (value) {
      this.value = value;
    }
  }

  validate(control: FormControl): ValidationErrors | null {
    return InputChipsComponent.authorsValidator(control, this.selectedItems);
  }
}
