import { Component, EventEmitter, forwardRef, HostBinding, Input, Output } from '@angular/core';
import { ControlValueAccessor, FormControl, NG_VALIDATORS, NG_VALUE_ACCESSOR, ValidationErrors, Validator } from '@angular/forms';
import * as moment from 'moment';

@Component({
  selector: 'app-datepicker',
  templateUrl: './datepicker.component.html',
  styleUrls: ['./datepicker.component.scss'],
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => DatepickerComponent),
      multi: true
    },
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DatepickerComponent),
      multi: true
    }
  ]
})
export class DatepickerComponent implements ControlValueAccessor, Validator {
  private _id = '';

  @HostBinding('attr.id') public externalId = '';

  @Input() public control: FormControl;
  @Input() public date: Date | moment.Moment;
  @Output() public dateChange = new EventEmitter();

  @Input('value') private _value: Date | moment.Moment | string;

  @Input()
  set id(value: string) {
    this._id = value;
    this.externalId = null;
  }

  get id() {
    return this._id;
  }

  static datepickerValidator(control: FormControl) {
    if (!DatepickerComponent.isDateFormatCorrect(control.value)) {
      return {incorrectDate: 'Please type date in "dd/MM/yyyy" format'};
    }
  }

  static isDateFormatCorrect(date: string) {
    if (date.indexOf('/') === -1) {
      return false;
    }

    const dateArr = date.split('/');

    if (
      isNaN(parseInt(dateArr[0], 10)) ||
      parseInt(dateArr[0], 10) < 1 ||
      parseInt(dateArr[0], 10) > 31 ||
      isNaN(parseInt(dateArr[1], 10)) ||
      parseInt(dateArr[1], 10) < 1 ||
      parseInt(dateArr[1], 10) > 12 ||
      isNaN(parseInt(dateArr[2], 10)) ||
      parseInt(dateArr[2], 10) < 1970 ||
      parseInt(dateArr[2], 10) > 2050
    ) {
      return false;
    }

    return true;
  }

  onChange: any = () => {};
  onTouched: any = () => {};

  get value() {
    return this._value;
  }

  set value(date) {
    this._value = date;
    this.onChange(date);
    this.onTouched();
  }

  constructor() { }

  setDate(): void {
    this._value = moment().format('D/MM/Y');
  }

  registerOnChange(fn): void {
    this.onChange = fn;
  }

  registerOnTouched(fn): void {
    this.onTouched = fn;
  }

  writeValue(value): void {
    if (value) {
      this.value = value;
    }
  }

  validate(control: FormControl): ValidationErrors | null {
    return DatepickerComponent.datepickerValidator(control);
  }
}
