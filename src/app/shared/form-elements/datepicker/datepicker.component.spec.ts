import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';

import { DatepickerComponent } from './datepicker.component';

describe('DatepickerComponent', () => {
  let component: DatepickerComponent;
  let fixture: ComponentFixture<DatepickerComponent>;
  let  dateChangeEventSpy;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatepickerComponent ],
      imports: [FormsModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatepickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    dateChangeEventSpy = spyOn(component.dateChange, 'emit');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit date when setDate', () => {
    component.setDate();
    expect(dateChangeEventSpy).toHaveBeenCalled();
  });
});
