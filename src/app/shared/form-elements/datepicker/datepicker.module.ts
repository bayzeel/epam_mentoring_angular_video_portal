import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { DpDatePickerModule } from 'ng2-date-picker';

import { DatepickerComponent } from './datepicker.component';

@NgModule({
  declarations: [
    DatepickerComponent,
  ],
  imports: [
    CommonModule,
    DpDatePickerModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    DatepickerComponent,
  ],
})
export class DatepickerModule { }
