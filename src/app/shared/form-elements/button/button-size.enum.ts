export enum ButtonSizeEnum {
  lg = 'lg',
  md = 'md',
  sm = 'sm',
}
