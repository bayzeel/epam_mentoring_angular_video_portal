import { Component, Input, OnInit } from '@angular/core';

import { ButtonSizeEnum } from './button-size.enum';
import { ButtonStyleEnum } from './button-style.enum';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {
  @Input() text: string;
  @Input() size: string;
  @Input() style: string;
  @Input() disabled: string;

  public buttonSize: typeof ButtonSizeEnum;
  public buttonStyle: typeof ButtonStyleEnum;

  constructor() { }

  ngOnInit() {
    this.buttonSize = ButtonSizeEnum;
    this.buttonStyle = ButtonStyleEnum;
  }

  stopPropagateSpanClick(event) {
    if (this.disabled) {
      event.stopPropagation();
    }
  }
}
