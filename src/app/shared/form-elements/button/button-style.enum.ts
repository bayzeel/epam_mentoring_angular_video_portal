export enum ButtonStyleEnum {
  default = 'default',
  primary = 'primary',
  success = 'success',
  link = 'link',
  inversed = 'inversed',
}
