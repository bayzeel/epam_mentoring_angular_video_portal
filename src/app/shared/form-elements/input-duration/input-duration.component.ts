import { Component, EventEmitter, forwardRef, HostBinding, Input, Output } from '@angular/core';
import { ControlValueAccessor, FormControl, NG_VALIDATORS, NG_VALUE_ACCESSOR, ValidationErrors, Validator } from '@angular/forms';

@Component({
  selector: 'app-input-duration',
  templateUrl: './input-duration.component.html',
  styleUrls: ['./input-duration.component.scss'],
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => InputDurationComponent),
      multi: true
    },
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputDurationComponent),
      multi: true
    }
  ]
})
export class InputDurationComponent implements ControlValueAccessor, Validator {
  private _id = '';

  @HostBinding('attr.id') public externalId = '';

  @Input() public control: FormControl;

  @Input() public duration: number;
  @Output() public durationChange = new EventEmitter();

  @Input('value') private _value: number;

  @Input()
  set id(value: string) {
    this._id = value;
    this.externalId = null;
  }

  get id() {
    return this._id;
  }

  static inputDurationValidator(control: FormControl) {
    if (!Number.isInteger(+control.value)) {
      return {notNumber: 'Only numbers allow'};
    }
  }

  onChange: any = () => {};
  onTouched: any = () => {};

  get value() {
    return this._value;
  }

  set value(value) {
    this._value = value;
    this.onChange(value);
    this.onTouched();
  }

  constructor() { }

  ngOnInit() { }

  setDuration(): void {
    this.durationChange.emit(this.duration);
  }

  registerOnChange(fn): void {
    this.onChange = fn;
  }

  registerOnTouched(fn): void {
    this.onTouched = fn;
  }

  writeValue(value): void {
    if (value) {
      this.value = value;
    }
  }

  validate(control: FormControl): ValidationErrors | null {
    return InputDurationComponent.inputDurationValidator(control);
  }
}
