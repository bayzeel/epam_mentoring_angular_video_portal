import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { InputDurationComponent } from './input-duration.component';



@NgModule({
  declarations: [
    InputDurationComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [InputDurationComponent]
})
export class InputDurationModule { }
