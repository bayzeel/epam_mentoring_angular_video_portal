import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';

import { InputDurationComponent } from './input-duration.component';

describe('InputDurationComponent', () => {
  let component: InputDurationComponent;
  let fixture: ComponentFixture<InputDurationComponent>;
  let durationChangeEventSpy;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InputDurationComponent ],
      imports: [FormsModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputDurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    durationChangeEventSpy = spyOn(component.durationChange, 'emit');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit date when setDuration', () => {
    component.setDuration();
    expect(durationChangeEventSpy).toHaveBeenCalled();
  });
});
