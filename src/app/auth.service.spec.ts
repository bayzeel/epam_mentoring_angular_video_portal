import { async, TestBed } from '@angular/core/testing';

import { AuthService } from './auth.service';

import { UserInterface } from './layout/header/user.interface';

describe('AuthService', () => {
  let service: AuthService;

  const user: UserInterface = {
    email: 'mail@mail.com',
    password: 'qwer1234',
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        AuthService,
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    service = TestBed.get(AuthService);

    let store = {};
    const mockLocalStorage = {
      getItem: (key: string): string => {
        return key in store ? store[key] : null;
      },
      setItem: (key: string, value: string) => {
        store[key] = `${value}`;
      },
      removeItem: (key: string) => {
        delete store[key];
      },
      clear: () => {
        store = {};
      }
    };

    spyOn(Storage.prototype, 'getItem').and.callFake(mockLocalStorage.getItem);
    spyOn(Storage.prototype, 'setItem').and.callFake(mockLocalStorage.setItem);
    spyOn(Storage.prototype, 'removeItem').and.callFake(mockLocalStorage.removeItem);
    spyOn(Storage.prototype, 'clear').and.callFake(mockLocalStorage.clear);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should store user data to the storage', () => {
    expect(service.isAuthenticated()).toBe(false);

    service.login(user);

    expect(localStorage.getItem('auth')).toBe(JSON.stringify(user));
  });

  it('should stop setting data to storage if it is already exist', () => {
    service.login(user);

    expect(service.login(user)).toBeUndefined();
  });

  it('should remove data from the storage', () => {
    expect(service.isAuthenticated()).toBe(false);

    service.login(user);

    expect(service.isAuthenticated()).toBe(true);

    service.logout();

    expect(localStorage.getItem('auth')).toBeNull();
  });

  it('should stop logging out if it already logged out', () => {
    expect(service.logout()).toBeUndefined();
  });

  it('should return valid data for authorized user', () => {
    service.login(user);

    expect(service.getUserInfo()).toEqual(user);
  });

  it('should return null for non authorized user', () => {
    expect(service.getUserInfo()).toBeNull();
  });
});
