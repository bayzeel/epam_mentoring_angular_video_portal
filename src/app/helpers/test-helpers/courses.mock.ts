import * as moment from 'moment';

import { CourseInterface } from '../../courses/course-list/course/course.interface';

export const coursesMock: CourseInterface[] = [
  {
    id: 101,
    title: 'Video Course 1. Name tag',
    creationDate: moment().subtract(12, 'days').startOf('day'),
    durationMin: 59,
    description: 'Learn about where you can find course descriptions, what information they include, how they work.',
    isTopRated: false,
  },
  {
    id: 102,
    title: 'Video Course 2. Name tag',
    creationDate: moment().add(1, 'days').startOf('day'),
    durationMin: 120,
    description: 'Learn about where you can find course descriptions, what information they include, how they work.',
    isTopRated: false,
  },
  {
    id: 103,
    title: 'Video Course 3. Name tag',
    creationDate: moment().subtract(15, 'days').startOf('day'),
    durationMin: 121,
    description: 'Learn about where you can find course descriptions, what information they include, how they work.',
    isTopRated: true,
  },
];
