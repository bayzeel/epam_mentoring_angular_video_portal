import { Action, createReducer, on } from '@ngrx/store';

import * as coursesActions from '../actions/courses.actions';
import { CoursesModelInterface } from '../models/courses.model';

const initialState: CoursesModelInterface = {
  courseList: [],
  stateMetadata: {
    isLoading: false,
    isLoaded: false,
    loadingError: null
  }
};

const reducer = createReducer(
  initialState,
  on(coursesActions.beginGetCoursesAction, (state: CoursesModelInterface) => {
    return {...state, stateMetadata: {isLoading: true, isLoaded: false, loadingError: null}};
  }),
  on(coursesActions.beginGetSearchCoursesAction, (state: CoursesModelInterface) => {
    return {...state, stateMetadata: {isLoading: true, isLoaded: false, loadingError: null}};
  }),
  on(coursesActions.beginAddCourseAction, (state: CoursesModelInterface) => {
    return {...state, stateMetadata: {isLoading: true, isLoaded: false, loadingError: null}};
  }),
  on(coursesActions.beginDeleteCourseAction, (state: CoursesModelInterface) => {
    return {...state, stateMetadata: {isLoading: true, isLoaded: false, loadingError: null}};
  }),
  on(coursesActions.beginUpdateCourseAction, (state: CoursesModelInterface) => {
    return {...state, stateMetadata: {isLoading: true, isLoaded: false, loadingError: null}};
  }),
  on(coursesActions.successGetCoursesAction, (state: CoursesModelInterface, { payload }) => {
    return {...state, courseList: [...state.courseList, ...payload], stateMetadata: {isLoading: false, isLoaded: true, loadingError: null}};
  }),
  on(coursesActions.clearCourseListAction, (state: CoursesModelInterface) => {
    return {...state, courseList: [], stateMetadata: {isLoading: false, isLoaded: false, loadingError: null}};
  }),
  on(coursesActions.errorCoursesAction, (state: CoursesModelInterface, error) => {
    return {...state, stateMetadata: {isLoading: false, isLoaded: true, loadingError: error}};
  }),
);

export function coursesReducer(state: CoursesModelInterface | undefined, action: Action) {
  return reducer(state, action);
}
