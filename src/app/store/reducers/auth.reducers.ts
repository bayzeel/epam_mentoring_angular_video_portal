import { Action, createReducer, on } from '@ngrx/store';

import * as authActions from '../actions/auth.actions';
import { AuthModelInterface } from '../models/auth.model';

const initialState: AuthModelInterface = {
  user: null,
  stateMetadata: {
    isLoading: false,
    isLoaded: false,
    loadingError: null
  }
};

const reducer = createReducer(
  initialState,
  on(authActions.beginGetUserTokenAction, (state: AuthModelInterface) => {
    return {...state, stateMetadata: {isLoading: true, isLoaded: false, loadingError: null}};
  }),
  on(authActions.beginGetUserInfoAction, (state: AuthModelInterface) => {
    return {...state, stateMetadata: {isLoading: true, isLoaded: false, loadingError: null}};
  }),
  on(authActions.successGetUserAction, (state: AuthModelInterface, { payload }) => {
    return {...state, user: payload, stateMetadata: {isLoading: false, isLoaded: true, loadingError: null}};
  }),
  on(authActions.errorUserTokensAction, (state: AuthModelInterface, error) => {
    return {...state, stateMetadata: {isLoading: false, isLoaded: true, loadingError: error}};
  }),
);

export function authReducer(state: AuthModelInterface | undefined, action: Action) {
  return reducer(state, action);
}
