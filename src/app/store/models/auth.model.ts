import { StateInterface } from '../state.interface';
import { UserInterface } from '../../layout/header/user.interface';

export interface AuthModelInterface extends StateInterface {
  user: UserInterface;
}
