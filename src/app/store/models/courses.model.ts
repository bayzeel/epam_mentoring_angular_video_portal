import { StateInterface } from '../state.interface';
import { CourseInterface } from '../../courses/course-list/course/course.interface';

export interface CoursesModelInterface extends StateInterface {
  courseList: CourseInterface[];
}
