export interface MetadataStateInterface {
  isLoading: boolean;
  isLoaded: boolean;
  loadingError: Error;
}
