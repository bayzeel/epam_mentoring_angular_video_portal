import { createAction, props } from '@ngrx/store';

import { CourseInterface } from '../../courses/course-list/course/course.interface';

export const beginGetCoursesAction = createAction(
  '[Courses] - Begin Get Courses'
);

export const beginGetSearchCoursesAction = createAction(
  '[Courses] - Begin Get Search Courses',
  props<{ textFragment: string }>()
);

export const beginAddCourseAction = createAction(
  '[Courses] - Begin Add Course',
  props<{ course: CourseInterface }>()
);

export const beginDeleteCourseAction = createAction(
  '[Courses] - Begin Delete Course',
  props<{ courseId: number }>()
);

export const beginUpdateCourseAction = createAction(
  '[Courses] - Begin Update Course',
  props<{ course: CourseInterface }>()
);

export const successGetCoursesAction = createAction(
  '[Courses] - Success Get Courses',
  props<{ payload: CourseInterface[] }>()
);

export const clearCourseListAction = createAction(
  '[Courses] - Clear Course List'
);

export const errorCoursesAction = createAction(
  '[Courses] - Error',
  props<Error>()
);
