import { createAction, props } from '@ngrx/store';

import { UserInterface } from '../../layout/header/user.interface';

export const beginGetUserTokenAction = createAction(
  '[Auth] - Begin Get User Token',
  props<{ user: UserInterface }>()
);

export const beginGetUserInfoAction = createAction(
  '[Auth] - Begin Get User Info'
);

export const successGetUserAction = createAction(
  '[Auth] - Success Get User Info',
  props<{ payload: UserInterface }>()
);

export const errorUserTokensAction = createAction(
  '[Auth] - Error',
  props<Error>()
);
