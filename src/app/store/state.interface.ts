import { MetadataStateInterface } from './metadata.state.interface';

export interface StateInterface {
  stateMetadata?: MetadataStateInterface;
  [key: string]: any;
}
