import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { map, catchError, tap, switchMap } from 'rxjs/operators';

import { CoursesService } from '../../courses.service';
import * as coursesActions from '../actions/courses.actions';
import { AppStateInterface } from '../app.state';

@Injectable()
export class CoursesEffects {
  beginGetCourses$ = createEffect(
    () => this._actions$.pipe(
      ofType('[Courses] - Begin Get Courses'),
      switchMap(() => this._coursesService.getPagedList()
        .pipe(
          map(courses => {
              this._store.dispatch(coursesActions.successGetCoursesAction({payload: courses}));
            }
          ),
          catchError(error => of(this._store.dispatch(coursesActions.errorCoursesAction(error))))
        )
      )
    ), {dispatch: false}
  );

  beginGetSearchCourses$ = createEffect(
    () => this._actions$.pipe(
      ofType('[Courses] - Begin Get Search Courses'),
      switchMap((action: any) => this._coursesService.search(action.textFragment)
        .pipe(
          map(courses => {
              this._store.dispatch(coursesActions.successGetCoursesAction({payload: courses}));
            }
          ),
          catchError(error => of(this._store.dispatch(coursesActions.errorCoursesAction(error))))
        )
      )
    ), {dispatch: false}
  );

  beginAddCourse$ = createEffect(
    () => this._actions$.pipe(
      ofType('[Courses] - Begin Add Course'),
      switchMap((action: any) => this._coursesService.createItem(action.course)
        .pipe(
          map(() => {
            this._router.navigate(['courses']);
            }
          ),
          catchError(error => of(this._store.dispatch(coursesActions.errorCoursesAction(error))))
        )
      )
    ), {dispatch: false}
  );

  beginDeleteCourse$ = createEffect(
    () => this._actions$.pipe(
      ofType('[Courses] - Begin Delete Course'),
      switchMap((action: any) => this._coursesService.deleteItem(action.courseId)
        .pipe(
          switchMap(() => this._coursesService.getPagedList()
            .pipe(
              map(courses => {
                  this._store.dispatch(coursesActions.successGetCoursesAction({payload: courses}));
                }
              ),
              catchError(error => of(this._store.dispatch(coursesActions.errorCoursesAction(error))))
            )
          ),
          catchError(error => of(this._store.dispatch(coursesActions.errorCoursesAction(error))))
        )
      )
    ), {dispatch: false}
  );

  beginUpdateCourse$ = createEffect(
    () => this._actions$.pipe(
      ofType('[Courses] - Begin Update Course'),
      switchMap((action: any) => this._coursesService.updateItem(action.course)
        .pipe(
          map(() => {
              this._router.navigate(['courses']);
            }
          ),
          catchError(error => of(this._store.dispatch(coursesActions.errorCoursesAction(error))))
        )
      )
    ), {dispatch: false}
  );

  errorCourses$ = createEffect(
    () => this._actions$.pipe(
      ofType('[Courses] - Error'),
      tap(error => {
        console.error(error);
      })
    ), {dispatch: false}
  );

  constructor(
    private readonly _actions$: Actions,
    private readonly _coursesService: CoursesService,
    private readonly _store: Store<AppStateInterface>,
    private readonly _router: Router,
  ) {}
}
