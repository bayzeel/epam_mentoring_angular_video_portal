import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { map, catchError, tap, switchMap } from 'rxjs/operators';

import * as authActions from '../actions/auth.actions';
import { AppStateInterface } from '../app.state';
import { AuthService } from '../../auth.service';

@Injectable()
export class AuthEffects {
  beginGetUserToken$ = createEffect(
    () => this._actions$.pipe(
      ofType('[Auth] - Begin Get User Token'),
      switchMap((action: any) => this._authService.login(action.user)
        .pipe(
          map((users: any[]) => {
            this._authService.setToken(users[0].token);
            this._authService.isAuth.next(this._authService.isAuthenticated());
            this._store.dispatch(authActions.successGetUserAction({payload: users[0]}));
            this._router.navigate(['courses']);
          }),
          catchError( error => of(authActions.errorUserTokensAction(error)))
        )
      )
    ), {dispatch: false}
  );

  beginGetUserInfo$ = createEffect(
    () => this._actions$.pipe(
      ofType('[Auth] - Begin Get User Info'),
      switchMap(() => this._authService.getUserInfo(this._authService.getUserToken())
        .pipe(
          map((users: any[]) => {
            this._store.dispatch(authActions.successGetUserAction({payload: users[0]}));
          }),
          catchError( error => of(authActions.errorUserTokensAction(error)))
        )
      )
    ), {dispatch: false}
  );

  errorAuth$ = createEffect(
    () => this._actions$.pipe(
      ofType('[Auth] - Error'),
      tap(error => {
        console.error(error);
      })
    ), {dispatch: false}
  );

  constructor(
    private readonly _actions$: Actions,
    private readonly _authService: AuthService,
    private readonly _store: Store<AppStateInterface>,
    private readonly _router: Router,
  ) {}
}
