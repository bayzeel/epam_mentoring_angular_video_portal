import { AuthModelInterface } from './models/auth.model';
import { CoursesModelInterface } from './models/courses.model';

export interface AppStateInterface {
  auth: AuthModelInterface;
  courses: CoursesModelInterface;
}
