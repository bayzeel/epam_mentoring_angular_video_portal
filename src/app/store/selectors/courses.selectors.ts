import { createSelector } from '@ngrx/store';

import { AppStateInterface } from '../app.state';

export const coursesSelector = createSelector(
  (state: AppStateInterface) => state,
  state => state.courses
);
