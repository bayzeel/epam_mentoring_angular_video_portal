import { createSelector } from '@ngrx/store';

import { AppStateInterface } from '../app.state';

export const authSelector = createSelector(
  (state: AppStateInterface) => state,
  state => state.auth
);
