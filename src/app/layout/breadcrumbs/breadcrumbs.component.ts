import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd, PRIMARY_OUTLET } from '@angular/router';
import { filter } from 'rxjs/operators';

import { CoursesService } from '../../courses.service';
import { BreadcrumbInterface } from './breadcrumb.interface';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-breadcrumbs',
  templateUrl: './breadcrumbs.component.html',
  styleUrls: ['./breadcrumbs.component.scss']
})
export class BreadcrumbsComponent implements OnInit, OnDestroy {
  private _routerSub: Subscription;
  public breadcrumbs: BreadcrumbInterface[] = [];

  constructor(
    private readonly _router: Router,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _coursesService: CoursesService,
  ) { }

  ngOnInit() {
    const noSubRoot: ActivatedRoute = this._activatedRoute.root;
    this.breadcrumbs = this._getBreadcrumbs(noSubRoot);

    this._routerSub = this._router.events
      .pipe(
        filter(event => event instanceof NavigationEnd)
      )
      .subscribe(() => {
        const root: ActivatedRoute = this._activatedRoute.root;

        this.breadcrumbs = this._getBreadcrumbs(root);
    });
  }

  private _getBreadcrumbs(
    route: ActivatedRoute,
    url: string = '',
    breadcrumbs: BreadcrumbInterface[] = [],
  ): BreadcrumbInterface[] {
    const ROUTE_DATA_BREADCRUMB = 'breadcrumb';
    const children: ActivatedRoute[] = route.children;

    if (children.length === 0) {
      return breadcrumbs;
    }

    for (const child of children) {
      if (child.outlet !== PRIMARY_OUTLET) {
        continue;
      }

      if (!child.snapshot.data.hasOwnProperty(ROUTE_DATA_BREADCRUMB)) {
        return this._getBreadcrumbs(child, url, breadcrumbs);
      }

      const routeURL: string = child.snapshot.url.map(segment => segment.path).join('/');

      url += `/${routeURL}`;

      const breadcrumb: BreadcrumbInterface = {
        label: child.snapshot.data[ROUTE_DATA_BREADCRUMB],
        params: child.snapshot.params,
        url,
      };

      if (child.snapshot.data[ROUTE_DATA_BREADCRUMB] === 'COURSE_ID') {
        this._coursesService.getCourseById(+child.snapshot.params.id).subscribe(course => {
          breadcrumb.label = course.title;
        });
      }

      if (child.snapshot.routeConfig.path) {
        breadcrumbs.push(breadcrumb);
      }

      return this._getBreadcrumbs(child, url, breadcrumbs);
    }

    return breadcrumbs;
  }

  ngOnDestroy(): void {
    if (this._routerSub) {
      this._routerSub.unsubscribe();
    }
  }
}
