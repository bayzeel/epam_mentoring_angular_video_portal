import { async, ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';

import { ButtonModule } from '../../shared/form-elements/button/button.module';

import { BreadcrumbsComponent } from '../breadcrumbs/breadcrumbs.component';
import { HeaderComponent } from './header.component';
import { FooterComponent } from '../footer/footer.component';
import { AuthService } from '../../auth.service';
import { UserInterface } from './user.interface';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;
  let authService: AuthService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        BreadcrumbsComponent,
        HeaderComponent,
        FooterComponent,
      ],
      imports: [
        ButtonModule,
        RouterTestingModule,
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    authService = TestBed.get(AuthService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should route to \'login\'', inject([Router], (router: Router) => {
    spyOn(router, 'navigate').and.stub();

    authService.logout();
    component.login();

    expect(router.navigate).toHaveBeenCalledWith(['login']);
  }));

  it('should output console.log', () => {
    const user: UserInterface = {
      email: 'mail@mail.com',
      password: 'qwer1234',
    };

    spyOn(console, 'log');

    authService.login(user);
    component.login();

    expect(console.log).toHaveBeenCalledWith('User is already logged in');
  });

  it('should route to \'login\' after logout', inject([Router], (router: Router) => {
    spyOn(router, 'navigate').and.stub();

    component.logout();

    expect(router.navigate).toHaveBeenCalledWith(['login']);
  }));
});
