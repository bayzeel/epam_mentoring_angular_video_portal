import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { AuthService } from '../../auth.service';
import { filter } from 'rxjs/operators';
import { AppStateInterface } from '../../store/app.state';
import { authSelector } from '../../store/selectors/auth.selectors';
import * as authActions from '../../store/actions/auth.actions';
import { AuthModelInterface } from '../../store/models/auth.model';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public authInfo$: Observable<AuthModelInterface>;
  public isUserAuthenticated: boolean;

  constructor(
    private readonly _router: Router,
    private readonly _authService: AuthService,
    private readonly _store: Store<AppStateInterface>,
  ) { }

  ngOnInit() {
    this._authService.isUserAuthenticated().subscribe(isAuth => {
      this.isUserAuthenticated = isAuth;

      if (this.isUserAuthenticated) {
        this._store.dispatch(authActions.beginGetUserInfoAction());
      }
    });

    this.authInfo$ = this._store.pipe(
      select(authSelector),
      filter(val => val !== undefined)
    );
  }

  logout(): void {
    this._authService.logout();
    this._router.navigate(['login']);
  }
}
