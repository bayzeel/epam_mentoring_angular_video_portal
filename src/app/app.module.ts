import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { AppRoutingModule } from './app-routing.module';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { CoursesModule } from './courses/courses.module';
import { NotFoundModule } from './not-found/not-found.module';
import { LayoutModule } from './layout/layout.module';
import { LoaderModule } from './shared/loader/loader.module';
import { LoginModule } from './login/login.module';

import { AppComponent } from './app.component';
import { TokenInterceptor } from './token.interceptor';

import { authReducer } from './store/reducers/auth.reducers';
import { coursesReducer } from './store/reducers/courses.reducers';
import { AuthEffects } from './store/effects/auth.effects';
import { CoursesEffects } from './store/effects/courses.effects';

@NgModule({
  declarations: [
    AppComponent,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    CoursesModule,
    FontAwesomeModule,
    NotFoundModule,
    LayoutModule,
    LoaderModule,
    LoginModule,
    StoreModule.forRoot({
      auth: authReducer,
      courses: coursesReducer
    }),
    EffectsModule.forRoot([
      AuthEffects,
      CoursesEffects
    ])
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
