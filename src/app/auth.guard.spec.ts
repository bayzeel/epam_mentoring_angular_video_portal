import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { AuthGuard } from './auth.guard';
import { AuthService } from './auth.service';

import { UserInterface } from './layout/header/user.interface';

describe('AuthGuard', () => {
  let authService: AuthService;
  let service: AuthGuard;

  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      RouterTestingModule,
    ],
  }));

  beforeEach(() => {
    authService = TestBed.get(AuthService);
    service = TestBed.get(AuthGuard);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return true for auth user', () => {
    const user: UserInterface = {
      email: 'mail@mail.com',
      password: 'qwer1234',
    };

    authService.login(user);

    expect(service.canActivate()).toBeTruthy();
  });

  it('should return false for non auth user', () => {
    authService.logout();

    expect(service.canActivate()).toBeFalsy();
  });
});
