import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import * as moment from 'moment';

import { CourseInterface } from './courses/course-list/course/course.interface';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class CoursesService {
  private _page: number = 1;

  public courses: CourseInterface[] = [];

  constructor(
    private readonly _httpClient: HttpClient,
  ) {}

  public getCourseById(id: number): Observable<any> {
    return this._httpClient.get(`http://localhost:3000/courses/${id}`);
  }

  public getList(): Observable<any> {
    return this._httpClient.get(`http://localhost:3000/courses`);
  }

  public getPagedList(limit: number = 3): Observable<any> {
    const options = {
      params: new HttpParams()
        .set('_page', '' + this._page)
        .set('_limit', '' + limit)
    };

    const res$ = this._httpClient.get(`http://localhost:3000/courses`, options);

    this._page++;

    return res$;
  }

  public search(searchText: string): Observable<any> {
    const options = {
      params: new HttpParams().set('q', searchText)
    };

    return this._httpClient.get(`http://localhost:3000/courses`, options);
  }

  public createItem(course: CourseInterface): Observable<any> {
    const newCourse: CourseInterface = {
      ...course,
      isTopRated: false,
    };

    return this._httpClient.post(`http://localhost:3000/courses`, newCourse);
  }

  public updateItem(course: CourseInterface): Observable<any> {
    return this._httpClient.patch(`http://localhost:3000/courses/${course.id}`, course);
  }

  public deleteItem(id: number): Observable<any> {
    return this._httpClient.delete(`http://localhost:3000/courses/${id}`);
  }

  public resetPageNumber(): void {
    this._page = 1;
  }
}
