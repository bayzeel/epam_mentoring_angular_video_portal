import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import * as jwtDecode from 'jwt-decode';

import { UserInterface } from './layout/header/user.interface';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  public isAuth = new BehaviorSubject<boolean>(this.isAuthenticated());

  constructor(
    private readonly _httpClient: HttpClient,
    private readonly _router: Router,
  ) { }

  public getToken(): string {
    return localStorage.getItem('auth');
  }

  public setToken(token: string): void {
    localStorage.setItem('auth', token);
  }

  public removeToken(): void {
    localStorage.removeItem('auth');
  }

  public isAuthenticated(): boolean {
    return !!this.getToken();
  }

  public isUserAuthenticated(): Observable<boolean> {
    return this.isAuth.asObservable();
  }

  public login(user: UserInterface): Observable<any> {
    if (this.isAuthenticated()) {
      return;
    }

    return this.getUserInfo(user);
  }

  public logout(): void {
    if (!this.isAuthenticated()) {
      return;
    }

    this.removeToken();
    this.isAuth.next(this.isAuthenticated());
  }

  public getUserToken(): UserInterface {
    return jwtDecode(this.getToken());
  }

  public getUserInfo(user: UserInterface): Observable<any> {
    return this._httpClient.get(`http://localhost:3000/users-token?email=${user.email}`);
  }
}
