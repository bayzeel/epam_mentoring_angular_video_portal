import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CourseListComponent } from './course-list/course-list.component';
import { CoursesComponent } from './courses.component';
import { EditCourseComponent } from './edit-course/edit-course.component';
import { AddCourseComponent } from './add-course/add-course.component';

import { CoursesGuard } from './courses.guard';

const routes: Routes = [
  {
    path: '',
    component: CoursesComponent,
    children: [
      {
        path: '',
        component: CourseListComponent,
      },
      {
        path: 'new',
        component: AddCourseComponent,
        data: {
          breadcrumb: 'New',
        },
      },
      {
        path: ':id',
        component: EditCourseComponent,
        canActivate: [CoursesGuard],
        data: {
          breadcrumb: 'COURSE_ID',
        },
      },
    ],
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ],
})
export class CoursesRoutingModule { }
