import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'orderBy'
})
export class OrderByPipe implements PipeTransform {
  transform(array: any, field: string, order: 'asc' | 'desc' = 'desc'): any[] {
    if (!array || !array.length) {
      return;
    }

    if (order === 'asc') {
      array.sort((a: any, b: any) => a[field] - b[field]);
      return array;
    }
    array.sort((a: any, b: any) => b[field] - a[field]);

    return array;
  }
}
