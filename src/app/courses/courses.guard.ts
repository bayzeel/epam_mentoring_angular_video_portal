import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

import { CoursesService } from '../courses.service';
import { CourseInterface } from './course-list/course/course.interface';
import { map } from 'rxjs/operators';

@Injectable()
export class CoursesGuard implements CanActivate {

  constructor(
    private readonly _coursesService: CoursesService,
    private readonly _router: Router,
  ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    return this._coursesService.getCourseById(+next.params.id)
      .pipe(
        map((course: CourseInterface) => {

          if (course && course.id) {
            return true;
          }

          this._router.navigate(['not-found']);
          return false;
        })
      );
  }
}
