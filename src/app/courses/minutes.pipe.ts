import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'minutes'
})
export class MinutesPipe implements PipeTransform {

  transform(value: number | string): string {
    if (!value) {
      return;
    }

    if (!Number.isInteger(+value)) {
      return;
    }

    const temp = +value;
    const hours = Math.floor( ( temp / 60 ) );
    const minutes: number = Math.floor( temp % 60 );

    return hours ? `${hours}h ${minutes}min` : `${minutes}min`;
  }
}
