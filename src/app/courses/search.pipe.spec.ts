import { SearchPipe } from './search.pipe';

describe('SearchPipe', () => {
  it('should search if searching array is strings array', () => {
    const pipe = new SearchPipe();
    let searchArr = ['apple', 'lemon', 'melon'];

    searchArr = pipe.transform(searchArr, 'melon');

    expect(searchArr.length).toBe(1);
    expect(searchArr[0]).toBe('melon');
  });

  it('should search if searching array is objects array', () => {
    const pipe = new SearchPipe();
    let searchArr = [
      {fruit: 'apple'},
      {fruit: 'lemon'},
      {fruit: 'melon'},
    ];

    searchArr = pipe.transform(searchArr, 'melon', 'fruit');

    expect(searchArr.length).toBe(1);
    expect(searchArr[0].fruit).toBe('melon');
  });

  it('should return null if searching array is nullable value', () => {
    const pipe = new SearchPipe();
    let searchArr = ['apple', 'lemon', 'melon'];

    searchArr = pipe.transform(searchArr);

    expect(searchArr.length).toBe(3);
  });
});
