import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {
  transform(value: any[], input?: string, field?: string): any[] {
    if (!input) {
      return value;
    }

    input = input.toLowerCase();

    return value.filter((item) => {
      if (field) {
        return JSON.stringify(item[field]).toLowerCase().includes(input);
      }

      return JSON.stringify(item).toLowerCase().includes(input);
    });
  }
}
