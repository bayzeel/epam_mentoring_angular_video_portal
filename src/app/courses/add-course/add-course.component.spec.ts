import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { ButtonModule } from '../../shared/form-elements/button/button.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { AddCourseComponent } from './add-course.component';
import { CoursesComponent } from '../courses.component';
import { CourseComponent } from '../course-list/course/course.component';
import { MinutesPipe } from '../minutes.pipe';
import { OrderByPipe } from '../order-by.pipe';
import { SearchboxComponent } from '../course-list/searchbox/searchbox.component';
import { DetectCourseCreationDateDirective } from '../course-list/course/detect-course-creation-date.directive';

describe('AddCourseComponent', () => {
  let component: AddCourseComponent;
  let fixture: ComponentFixture<AddCourseComponent>;
  let initAddEventSpy;
  let saveEventSpy;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
      ],
      declarations: [
        AddCourseComponent,
        CourseComponent,
        CoursesComponent,
        MinutesPipe,
        OrderByPipe,
        SearchboxComponent,
        DetectCourseCreationDateDirective,
      ],
      imports: [
        ButtonModule,
        FontAwesomeModule,
        FormsModule,
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCourseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    initAddEventSpy = spyOn(component.cancel, 'emit');
    saveEventSpy = spyOn(component.save, 'emit');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit empty event when cancelAdding runs', () => {
    component.cancelAdding();
    expect(initAddEventSpy).toHaveBeenCalled();
  });

  it('should emit event when saveCourse', () => {
    component.saveCourse();
    expect(saveEventSpy).toHaveBeenCalled();
  });
});
