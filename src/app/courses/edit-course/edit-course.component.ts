import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';

import { AuthorInterface } from '../author.interface';
import { AuthorsService } from '../authors.service';
import { CoursesService } from '../../courses.service';
import { CourseInterface } from '../course-list/course/course.interface';
import { AppStateInterface } from '../../store/app.state';
import * as coursersActions from '../../store/actions/courses.actions';
import { DatepickerComponent } from '../../shared/form-elements/datepicker/datepicker.component';
import { InputDurationComponent } from '../../shared/form-elements/input-duration/input-duration.component';
import { InputChipsComponent } from '../../shared/form-elements/input-chips/input-chips.component';

@Component({
  selector: 'app-edit-course',
  templateUrl: './edit-course.component.html',
  styleUrls: ['./edit-course.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EditCourseComponent implements OnInit, OnDestroy {
  private _authorsSearchSub: Subscription;
  private _course: CourseInterface;
  public durationMin: number;
  public authors: AuthorInterface[];
  public searchedAuthors: AuthorInterface[];
  public selectedAuthors: AuthorInterface[];
  public form: FormGroup;

  constructor(
    private readonly _router: Router,
    private readonly _cdr: ChangeDetectorRef,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _coursesService: CoursesService,
    private readonly _authorsService: AuthorsService,
    private readonly _store: Store<AppStateInterface>,
    private readonly _fb: FormBuilder,
  ) {
    this.createForm();
  }

  ngOnInit() {
    this._coursesService.getCourseById(+this._activatedRoute.snapshot.params.id).subscribe((course: CourseInterface) => {
      this._course = course;
      const date = moment(this._course.creationDate).format('D/MM/Y');
      this.authors = [...this._course.authors];
      this.form.patchValue({
        title: this._course.title,
        description: this._course.description,
        creationDate: date,
        durationMin: this._course.durationMin
      });
      this.form.get('durationMin').setValue('' + this._course.durationMin);
      this.form.get('authors').markAsTouched();
      this._cdr.detectChanges();
    });
  }

  public saveCourse(): void {
    const dateArr: string[] = this.form.controls.creationDate.value.split('/');
    const newDate = new Date(parseInt(dateArr[2], 10), parseInt(dateArr[1], 10) - 1, parseInt(dateArr[0], 10));
    const course: CourseInterface = {
      ...this._course,
      title: this.form.controls.title.value,
      description: this.form.controls.description.value,
      creationDate: newDate,
      durationMin: this.form.controls.durationMin.value,
      authors: this.selectedAuthors
    };

    this._store.dispatch(coursersActions.beginUpdateCourseAction({ course }));
  }

  public cancelEdit(): void {
    this._router.navigate(['../'], {relativeTo: this._activatedRoute});
  }

  public searchAuthors(textFragment: string) {
    this._authorsSearchSub = this._authorsService.search(textFragment).subscribe(authors => {
      this.searchedAuthors = [...authors];
      this._cdr.markForCheck();
    });
  }

  public createForm() {
    this.form = this._fb.group( {
      title: ['', [Validators.required, Validators.maxLength(50)]],
      description: ['', [Validators.required, Validators.maxLength(500)]],
      creationDate: ['', [Validators.required, DatepickerComponent.datepickerValidator]],
      durationMin: ['', [Validators.required, InputDurationComponent.inputDurationValidator]],
      authors: ['', [InputChipsComponent.authorsValidator]]
    } );
  }

  public getSelectedItems(authors: AuthorInterface[]) {
    this.selectedAuthors = [...authors];
  }

  ngOnDestroy() {
    if (this._authorsSearchSub) {
      this._authorsSearchSub.unsubscribe();
    }
  }
}
