import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { ButtonModule } from '../shared/form-elements/button/button.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { AddCourseComponent } from './add-course/add-course.component';
import { CoursesComponent } from './courses.component';
import { CourseComponent } from './course-list/course/course.component';
import { MinutesPipe } from './minutes.pipe';
import { OrderByPipe } from './order-by.pipe';
import { SearchboxComponent } from './course-list/searchbox/searchbox.component';
import { DetectCourseCreationDateDirective } from './course-list/course/detect-course-creation-date.directive';

import { CoursesService } from '../courses.service';
import { coursesMock } from '../helpers/test-helpers/courses.mock';

import { NewCourseInterface } from './add-course/new-course.interface';

describe('CoursesComponent', () => {
  let component: CoursesComponent;
  let fixture: ComponentFixture<CoursesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
      ],
      declarations: [
        AddCourseComponent,
        CourseComponent,
        CoursesComponent,
        MinutesPipe,
        OrderByPipe,
        SearchboxComponent,
        DetectCourseCreationDateDirective,
      ],
      providers: [
        CoursesService,
      ],
      imports: [
        ButtonModule,
        FontAwesomeModule,
        FormsModule,
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoursesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    spyOn(window.console, 'log');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should output console.log when initAddCourse runs', () => {
    component.initAddCourse();
    expect(window.console.log).toHaveBeenCalledWith('Add course');
  });

  it('should filter courses', () => {
    component.courses = [...coursesMock];
    component.searchCourse('3');
    expect(component.searchCourses.length).toBe(1);
  });

  it('should output console.log when editCourse runs', () => {
    component.editCourse(1);
    expect(window.console.log).toHaveBeenCalledWith('Edit course:', 1);
  });

  it('should output console.log when deleteCourse runs', () => {
    component.deleteCourse(coursesMock[0]);
    expect(component.courses.length).toBe(2);
  });

  it('should output console.log when addMore runs', () => {
    component.addMore();
    expect(window.console.log).toHaveBeenCalledWith('Load more courses');
  });

  it('should open deleting modal window', () => {
    component.openDeleteCourseModal(coursesMock[0]);
    expect(component.isDeleteCourseDialogOpened).toBeTruthy();
  });

  it('should close deleting modal window', () => {
    component.closeDeleteCourseModal();
    expect(component.isDeleteCourseDialogOpened).toBeFalsy();
  });

  it('should close adding course modal window', () => {
    component.cancelAddingCourse();
    expect(component.isAddCourseFormOpened).toBeFalsy();
  });

  it('should output console.log when saveNewCourse runs', () => {
    component.saveNewCourse({} as NewCourseInterface);
    expect(window.console.log).toHaveBeenCalledWith('Save new course');
  });
});
