import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { ButtonModule } from '../shared/form-elements/button/button.module';
import { DatepickerModule } from '../shared/form-elements/datepicker/datepicker.module';
import { InputChipsModule } from '../shared/form-elements/input-chips/input-chips.module';
import { InputDurationModule } from '../shared/form-elements/input-duration/input-duration.module';
import { ModalModule } from '../shared/modal/modal.module';
import { LoaderModule } from '../shared/loader/loader.module';
import { CoursesRoutingModule } from './courses-routing.module';

import { AddCourseComponent } from './add-course/add-course.component';
import { EditCourseComponent } from './edit-course/edit-course.component';
import { CoursesComponent } from './courses.component';
import { CourseComponent } from './course-list/course/course.component';
import { CourseListComponent } from './course-list/course-list.component';
import { SearchboxComponent } from './course-list/searchbox/searchbox.component';
import { MinutesPipe } from './minutes.pipe';
import { OrderByPipe } from './order-by.pipe';
import { SearchPipe } from './search.pipe';
import { DetectCourseCreationDateDirective } from './course-list/course/detect-course-creation-date.directive';

import { AuthorsService } from './authors.service';
import { CoursesGuard } from './courses.guard';

@NgModule({
  declarations: [
    AddCourseComponent,
    CourseComponent,
    CoursesComponent,
    CourseListComponent,
    SearchboxComponent,
    MinutesPipe,
    OrderByPipe,
    SearchPipe,
    DetectCourseCreationDateDirective,
    EditCourseComponent,
  ],
  providers: [
    AuthorsService,
    CoursesGuard,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    CoursesRoutingModule,
    FontAwesomeModule,
    HttpClientModule,
    ButtonModule,
    DatepickerModule,
    InputChipsModule,
    ModalModule,
    LoaderModule,
    InputDurationModule
  ],
  exports: [
    CoursesComponent,
  ],
})
export class CoursesModule { }
