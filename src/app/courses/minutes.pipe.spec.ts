import { MinutesPipe } from './minutes.pipe';

describe('MinutesPipe', () => {
  it('should transform datestring', () => {
    const minutesPipe = new MinutesPipe();

    expect(minutesPipe.transform(125)).toBe('2h 5min');
  });

  it('should return if value is not integer', () => {
    const minutesPipe = new MinutesPipe();

    expect(minutesPipe.transform('q')).toBeUndefined();
  });

  it('should display only minutes if value less than 60', () => {
    const minutesPipe = new MinutesPipe();

    expect(minutesPipe.transform(25)).toBe('25min');
  });
});
