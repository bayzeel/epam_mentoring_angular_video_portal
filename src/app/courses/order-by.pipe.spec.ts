import { OrderByPipe } from './order-by.pipe';
import { async } from '@angular/core/testing';

describe('OrderByPipe', () => {
  let pipe: OrderByPipe;
  let users = [];

  beforeEach(async(() => {
    pipe = new OrderByPipe();
    users = [
      {name: 'Roy', age: 35},
      {name: 'Jay', age: 25},
      {name: 'Sam', age: 30},
    ];
  }));

  it('should sort array by descending order', () => {
    users = pipe.transform(users, 'age');

    expect(users[0].age).toBe(35);
  });

  it('should sort array by ascending order', () => {
    users = pipe.transform(users, 'age', 'asc');

    expect(users[0].age).toBe(25);
  });
});
