import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class AuthorsService {

  constructor(
    private readonly _httpClient: HttpClient,
  ) { }

  public search(searchText: string): Observable<any> {
    const options = {
      params: new HttpParams().set('q', searchText)
    };

    return this._httpClient.get(`http://localhost:3000/authors`, options);
  }
}
