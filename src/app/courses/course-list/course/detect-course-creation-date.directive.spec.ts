import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import * as moment from 'moment';

import { Component, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { DetectCourseCreationDateDirective } from './detect-course-creation-date.directive';

const greenHighlightDate = moment().subtract(12, 'days').startOf('day');
const blueHighlightDate = moment().add(1, 'days').startOf('day');
const noHighlightDate = moment().subtract(15, 'days').startOf('day');

@Component({
  template: `
  <div style="border: 1px solid white;" [appDetectCourseDate]="greenHighlightDate">Green</div>
  <div style="border: 1px solid white;" [appDetectCourseDate]="blueHighlightDate">Blue</div>
  <div style="border: 1px solid white;" [appDetectCourseDate]="noHighlightDate">White</div>
  `
})
class DetectCourseCreationDateTestComponent { }

describe('DetectCourseCreationDateDirective', () => {
  let fixture: ComponentFixture<DetectCourseCreationDateTestComponent>;
  let des;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
      ],
      declarations: [
        DetectCourseCreationDateDirective,
        DetectCourseCreationDateTestComponent,
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(DetectCourseCreationDateTestComponent);
    fixture.detectChanges();

    des = fixture.debugElement.queryAll(By.directive(DetectCourseCreationDateDirective));
  }));

  it('should have three elements', () => {
    expect(des.length).toBe(3);
  });

  // it('should border color 1st element green', () => {
  //   fixture.detectChanges();
  //   fixture.whenStable().then(() => {
  //     const borderColor = des[0].nativeElement.style.borderColor;
  //     expect(borderColor).toBe('green');
  //   });
  // });

  // it('should border color 2st element blue', () => {
  //   fixture.detectChanges();
  //   fixture.whenStable().then(() => {
  //     const borderColor = des[1].nativeElement.style.borderColor;
  //     expect(borderColor).toBe('blue');
  //   });
  // });

  // it('should border color 3st element white', fakeAsync(() => {
  //   fixture.detectChanges();
  //   fixture.whenStable().then(() => {
  //     const borderColor = des[2].nativeElement.style.borderColor;
  //     expect(borderColor).toBe('white');
  //   });
  // }));
});
