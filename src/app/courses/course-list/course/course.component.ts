import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import { IconDefinition, faCalendar, faClock, faPencilAlt, faTrash } from '@fortawesome/free-solid-svg-icons';

import { CourseInterface } from './course.interface';

@Component({
  selector: 'app-course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CourseComponent implements CourseInterface, OnInit {
  public calendarIcon: IconDefinition = faCalendar;
  public clockIcon: IconDefinition = faClock;
  public deleteIcon: IconDefinition = faTrash;
  public editIcon: IconDefinition = faPencilAlt;

  @Input() public id: number;
  @Input() public title: string;
  @Input() public creationDate: Date | moment.Moment;
  @Input() public durationMin: number;
  @Input() public description: string;
  @Input() public isTopRated: boolean;

  @Output() public delete = new EventEmitter<CourseInterface>();

  constructor(
    private readonly _router: Router,
    private readonly _activatedRoute: ActivatedRoute,
  ) {}

  ngOnInit() {}

  public editCourse(): void {
    this._router.navigate([`${this.id}`], {relativeTo: this._activatedRoute});
  }

  public deleteCourse(): void {
    const course: CourseInterface = {
      id: this.id,
      title: this.title,
      creationDate: this.creationDate,
      durationMin: this.durationMin,
      description: this.description,
      isTopRated: this.isTopRated,
    };

    this.delete.emit(course);
  }
}
