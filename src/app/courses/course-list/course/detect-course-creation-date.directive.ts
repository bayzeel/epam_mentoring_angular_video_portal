import { Directive, ElementRef, Input, OnInit} from '@angular/core';
import * as moment from 'moment';

@Directive({
  selector: '[appDetectCourseDate]'
})
export class DetectCourseCreationDateDirective implements OnInit {
  private _freshCourseBorderColor = 'green';
  private _releaseCourseBorderColor = 'blue';
  @Input('appDetectCourseDate') creationDate: Date | moment.Moment;

  constructor(
    private readonly _el: ElementRef
  ) { }

  ngOnInit() {
    this._addBorderColor();
  }

  private _addBorderColor(): void {
    const currentDate = moment();
    const twoWeeksAgo = moment().subtract(14, 'days').startOf('day');

    if (
      moment(this.creationDate).isBefore(currentDate) &&
      (moment(this.creationDate).isAfter(twoWeeksAgo) || moment(this.creationDate).isSame(twoWeeksAgo))
    ) {
      this._el.nativeElement.style.borderColor = this._freshCourseBorderColor;
    } else if (moment(this.creationDate).isAfter(currentDate)) {
      this._el.nativeElement.style.borderColor = this._releaseCourseBorderColor;
    }
  }
}
