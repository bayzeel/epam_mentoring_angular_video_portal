import * as moment from 'moment';

import { AuthorInterface } from '../../author.interface';

export interface CourseInterface {
  id?: number;
  title: string;
  creationDate: Date | moment.Moment;
  durationMin: number;
  description: string;
  isTopRated?: boolean;
  authors?: AuthorInterface[];
}
