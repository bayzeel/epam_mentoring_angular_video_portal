import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { ButtonModule } from '../../../shared/form-elements/button/button.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { AddCourseComponent } from '../../add-course/add-course.component';
import { CoursesComponent } from '../../courses.component';
import { CourseComponent } from './course.component';
import { MinutesPipe } from '../../minutes.pipe';
import { OrderByPipe } from '../../order-by.pipe';
import { SearchboxComponent } from '../searchbox/searchbox.component';
import { DetectCourseCreationDateDirective } from './detect-course-creation-date.directive';

import { coursesMock } from '../../../courses.service';

describe('CourseComponent', () => {
  let component: CourseComponent;
  let fixture: ComponentFixture<CourseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
      ],
      declarations: [
        AddCourseComponent,
        CourseComponent,
        CoursesComponent,
        MinutesPipe,
        OrderByPipe,
        SearchboxComponent,
        DetectCourseCreationDateDirective,
      ],
      imports: [
        ButtonModule,
        FontAwesomeModule,
        FormsModule,
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit id when editCourse runs', () => {
    const editEventSpy = spyOn(component.edit, 'emit');

    component.id = 1;
    component.editCourse();
    expect(editEventSpy).toHaveBeenCalledWith(1);
  });

  it('should emit id when deleteCourse runs', () => {
    const deleteEventSpy = spyOn(component.delete, 'emit');

    component.id = coursesMock[0].id;
    component.title = coursesMock[0].title;
    component.creationDate = coursesMock[0].creationDate;
    component.durationMin = coursesMock[0].durationMin;
    component.description = coursesMock[0].description;
    component.isTopRated = coursesMock[0].isTopRated;

    const course = {
      id: component.id,
      title: component.title,
      creationDate: component.creationDate,
      durationMin: component.durationMin,
      description: component.description,
      isTopRated: component.isTopRated,
    };

    component.deleteCourse();
    expect(deleteEventSpy).toHaveBeenCalledWith(course);
  });
});
