import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { ButtonModule } from '../../../shared/form-elements/button/button.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { AddCourseComponent } from '../../add-course/add-course.component';
import { CoursesComponent } from '../../courses.component';
import { CourseComponent } from '../course/course.component';
import { MinutesPipe } from '../../minutes.pipe';
import { OrderByPipe } from '../../order-by.pipe';
import { SearchboxComponent } from './searchbox.component';
import { DetectCourseCreationDateDirective } from '../course/detect-course-creation-date.directive';

describe('SearchboxComponent', () => {
  let component: SearchboxComponent;
  let fixture: ComponentFixture<SearchboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
      ],
      declarations: [
        AddCourseComponent,
        CourseComponent,
        CoursesComponent,
        MinutesPipe,
        OrderByPipe,
        SearchboxComponent,
        DetectCourseCreationDateDirective,
      ],
      imports: [
        ButtonModule,
        FontAwesomeModule,
        FormsModule,
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit event when findCourse runs', () => {
    const searchSpy = spyOn(component.search, 'emit');

    component.findCourse();
    expect(searchSpy).toHaveBeenCalled();
  });
});
