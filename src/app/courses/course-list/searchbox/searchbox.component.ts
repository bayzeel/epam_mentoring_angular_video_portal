import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { Subject, Subscription } from 'rxjs';
import { bufferCount, map } from 'rxjs/operators';

@Component({
  selector: 'app-searchbox',
  templateUrl: './searchbox.component.html',
  styleUrls: ['./searchbox.component.scss']
})
export class SearchboxComponent implements OnInit, OnDestroy {
  private _searchTextSub: Subscription;

  @Output() public search = new EventEmitter<string>();
  @Output() public filterSearch = new Subject<string>();
  public searchText = '';

  constructor() { }

  ngOnInit() {
    this._searchTextSub = this.filterSearch
      .pipe(
        bufferCount(3),
        map(textArr => textArr[textArr.length - 1])
      )
      .subscribe(text => {
        this.search.emit(text);
    });
  }

  findCourse() {
    this.filterSearch.next(this.searchText);
  }

  ngOnDestroy() {
    if (this._searchTextSub) {
      this._searchTextSub.unsubscribe();
    }
  }
}
