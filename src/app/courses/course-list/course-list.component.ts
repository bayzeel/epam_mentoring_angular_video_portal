import { Component, OnDestroy, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { IconDefinition, faPlus } from '@fortawesome/free-solid-svg-icons';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';

import { CourseInterface } from './course/course.interface';
import { SearchPipe } from '../search.pipe';
import { CoursesService } from '../../courses.service';
import { AppStateInterface } from '../../store/app.state';
import { coursesSelector } from '../../store/selectors/courses.selectors';
import * as coursersActions from '../../store/actions/courses.actions';
import { CoursesModelInterface } from '../../store/models/courses.model';

@Component({
  selector: 'app-course-list',
  templateUrl: './course-list.component.html',
  styleUrls: ['./course-list.component.scss'],
  providers: [SearchPipe],
})
export class CourseListComponent implements OnInit, OnDestroy {
  public coursesState: CoursesModelInterface;
  public isDeleteCourseDialogOpened: boolean;
  public courseUnderDeleting: CourseInterface;
  public faPlus: IconDefinition = faPlus;
  public isMoreButtonActive = true;
  private _coursesStateSub: Subscription;

  constructor(
    private readonly _router: Router,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _searchPipe: SearchPipe,
    private readonly _coursesService: CoursesService,
    private readonly _store: Store<AppStateInterface>,
  ) {}

  ngOnInit() {
    this._coursesStateSub = this._store.pipe(
      select(coursesSelector),
      filter(val => val !== undefined)
    ).subscribe(coursesState => {
      this.coursesState = coursesState;
    });

    this._store.dispatch(coursersActions.beginGetCoursesAction());
  }
  ngOnDestroy() {
    this._coursesService.resetPageNumber();
    this._store.dispatch(coursersActions.clearCourseListAction());

    if (this._coursesStateSub) {
      this._coursesStateSub.unsubscribe();
    }
  }

  searchCourse(textFragment: string): void {
    if (textFragment) {
      this.coursesState = null;
      this.isMoreButtonActive = false;
      this._store.dispatch(coursersActions.clearCourseListAction());
      this._store.dispatch(coursersActions.beginGetSearchCoursesAction({ textFragment }));
    } else {
      this._coursesService.resetPageNumber();
      this.coursesState = null;
      this.isMoreButtonActive = true;
      this._store.dispatch(coursersActions.clearCourseListAction());
      this._store.dispatch(coursersActions.beginGetCoursesAction());
    }
  }

  initAddCourse(): void {
    this._router.navigate(['new'], {relativeTo: this._activatedRoute});
  }

  deleteCourse(course: CourseInterface): void {
    this._coursesService.resetPageNumber();
    this.coursesState = null;
    this.isDeleteCourseDialogOpened = false;
    this._store.dispatch(coursersActions.clearCourseListAction());
    this._store.dispatch(coursersActions.beginDeleteCourseAction({ courseId: course.id }));
  }

  openDeleteCourseModal(course: CourseInterface): void {
    this.isDeleteCourseDialogOpened = true;
    this.courseUnderDeleting = course;
  }

  closeDeleteCourseModal(): void {
    this.isDeleteCourseDialogOpened = false;
  }

  addMore(): void {
    this._store.dispatch(coursersActions.beginGetCoursesAction());
  }
}
