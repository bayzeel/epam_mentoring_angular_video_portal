import { TestBed } from '@angular/core/testing';
import * as moment from 'moment';
import { CoursesService } from './courses.service';

describe('CoursesService', () => {
  let service: CoursesService;
  const addCourse = {
    id: 104,
    title: 'Video Course 3. Name tag',
    creationDate: moment().subtract(15, 'days').startOf('day'),
    durationMin: 121,
    description: 'Learn about where you can find course descriptions, what information they include, how they work.',
    isTopRated: true,
  };
  const updCourse = {
    id: 101,
    title: 'Video Course 1',
    creationDate: moment().subtract(12, 'days').startOf('day'),
    durationMin: 59,
    description: 'Learn about where you can find course descriptions, what information they include, how they work.',
    isTopRated: false,
  };
  const titleBeforeUpd  = 'Video Course 1. Name tag';
  const titleAfterUpd  = 'Video Course 1';

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        CoursesService,
      ],
    });
  });
  beforeEach( () => {
    service = TestBed.get(CoursesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return 3 courses', () => {
    expect(service.getList().length).toBe(3);
  });

  it('should return 4 courses after adding 4-th', () => {
    service.getList();
    service.createItem(addCourse);

    expect(service.courses.length).toBe(4);
  });

  it('should return 4 courses after adding 4-th', () => {
    service.getList();

    expect(service.courses[0].title).toBe(titleBeforeUpd);

    service.updateItem(updCourse);

    expect(service.courses[0].title).toBe(titleAfterUpd);
  });

  it('should return null if updating course id doesn\'t exist', () => {
    service.getList();

    expect(service.updateItem(addCourse)).toBeNull();
  });

  it('should return 2 courses after deleting 3-rd', () => {
    service.getList();

    expect(service.courses.length).toBe(3);

    service.deleteItem(updCourse);

    expect(service.courses.length).toBe(2);
  });

  it('should return null if deleting course id doesn\'t exist', () => {
    service.getList();

    expect(service.deleteItem(addCourse)).toBeNull();
  });

  it('should return null if deleting course id doesn\'t exist', () => {
    service.getList();

    service.updateItem(updCourse);

    expect(service.getItemById(updCourse)).toBe(updCourse);
  });

  it('should return null if getting by id course doesn\'t exist', () => {
    service.getList();

    expect(service.getItemById(addCourse)).toBeNull();
  });
});
