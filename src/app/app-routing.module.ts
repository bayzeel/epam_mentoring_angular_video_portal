import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AlreadyAuthGuard } from './already-auth.guard';
import { AuthGuard } from './auth.guard';

const rootRoutes: Routes = [
  {
    path: 'courses',
    loadChildren: () => import('./courses/courses.module').then(mod => mod.CoursesModule),
    canActivate: [AuthGuard],
    data: {
      breadcrumb: 'Courses',
    },
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then(mod => mod.LoginModule),
    canActivate: [AlreadyAuthGuard],
  },
  {
    path: 'not-found',
    loadChildren: () => import('./not-found/not-found.module').then( mod => mod.NotFoundModule),
  },
  {path: '**', redirectTo: 'not-found'},
];

@NgModule({
  imports: [
    RouterModule.forRoot( rootRoutes )
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
