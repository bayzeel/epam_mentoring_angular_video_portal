import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { AlreadyAuthGuard } from './already-auth.guard';
import { AuthService } from './auth.service';

import { UserInterface } from './layout/header/user.interface';

describe('AlreadyAuthGuard', () => {
  let authService: AuthService;
  let service: AlreadyAuthGuard;

  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      RouterTestingModule,
    ],
  }));

  beforeEach(() => {
    authService = TestBed.get(AuthService);
    service = TestBed.get(AlreadyAuthGuard);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return false for auth user', () => {
    const user: UserInterface = {
      email: 'mail@mail.com',
      password: 'qwer1234',
    };

    authService.login(user);

    expect(service.canActivate()).toBeFalsy();
  });

  it('should return true for non auth user', () => {
    authService.logout();

    expect(service.canActivate()).toBeTruthy();
  });
});
