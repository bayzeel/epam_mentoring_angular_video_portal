import { TestBed, async, ComponentFixture } from '@angular/core/testing';

import { AppRoutingModule } from './app-routing.module';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { CoursesModule } from './courses/courses.module';
import { LayoutModule } from './layout/layout.module';
import { LoginModule } from './login/login.module';

import { AppComponent } from './app.component';
import { AuthService } from './auth.service';

describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  let authService: AuthService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
      ],
      imports: [
        AppRoutingModule,
        CoursesModule,
        FontAwesomeModule,
        LayoutModule,
        LoginModule,
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    authService = TestBed.get(AuthService);
  });

  it('should create the app', () => {
    const app = fixture.debugElement.componentInstance;

    expect(app).toBeTruthy();
  });

  it('should get isAuth data', () => {
    component.ngOnInit();
    authService.logout();

    component.isUserAuthenticated.subscribe( (val) => {
      expect(val as boolean).toBeFalsy();
    });
  });
});
